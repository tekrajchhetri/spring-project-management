# Project Management 

A simple web based project management application using Spring Framework.

## Getting Started

This is a simple project management application (in development), using Java Spring Framework.
### Prerequisites

* Java >=8
* PostgreSQL 

## Running the tests

Clone this repository and open as maven project then all the required dependencies will be setup (suggested to use Intellij). For production scenario use postgres database and create the table using the following script. In application.properties, change the name of db as necessary.


```
CREATE SEQUENCE IF NOT EXISTS employee_seq;

CREATE TABLE IF NOT EXISTS employee (

employeeid BIGINT NOT NULL DEFAULT nextval('employee_seq') PRIMARY KEY,
email VARCHAR(100) NOT NULL,
firstname VARCHAR(100) NOT NULL,
lastname VARCHAR(100) NOT NULL

);

CREATE SEQUENCE IF NOT EXISTS project_seq;

CREATE TABLE IF NOT EXISTS project (

projectid BIGINT NOT NULL DEFAULT nextval('project_seq') PRIMARY KEY,
name VARCHAR(100) NOT NULL,
stage VARCHAR(100) NOT NULL,
description VARCHAR(500) NOT NULL

);


CREATE TABLE IF NOT EXISTS project_employer (

project_id BIGINT REFERENCES project,
employee_id BIGINT REFERENCES employee

);
```

### Testing

You can run the test scripts present in test.

### Deployments

![](images/home.png)
![](images/employee.png)
![](images/assign.png)

 

## Built With

* [Java Spring](https://spring.io/) - The web framework used
* [Maven](https://maven.apache.org/) - Dependency Management
* [Bootstrap](https://getbootstrap.com/) - Front-end component library.
* [H2](https://www.h2database.com/html/main.html) - H2 in-memory database for development.
* [PostgreSQL](https://www.postgresql.org/) - PostgreSQL for deployment and integration testing.



