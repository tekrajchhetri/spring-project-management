-- INSERT EMPLOYEES
insert into employee (employeeid, firstname, lastname, email) values (nextval('employee_seq'), 'John', 'Warton', 'warton@gmail.com');

-- INSERT PROJECTS
insert into project (projectid, name, stage, description) values (nextval('project_seq'), 'Large Production Deploy', 'NOTSTARTED', 'This requires all hands on deck for the final deployment of the software into production');

-- INSERT project_employer_RELATION
insert into project_employer (employee_id, project_id) (select e.employeeid, p.projectid from employee e, project p where e.lastname ='Warton' AND p.name = 'Large Production Deploy');
