var chartData = decodeHTML(chartData);
var chartJsonArray = JSON.parse(chartData);
var arylength = chartJsonArray.length;

var numericData=[];
var label=[];

for(var i=0; i < arylength; i++){
    numericData[i] = chartJsonArray[i].projectCount;
    label[i] = chartJsonArray[i].stage;
}
var ctx = document.getElementById('pichart').getContext('2d');
var chart = new Chart(ctx, {
    // The type of chart we want to create
    type: 'pie',

    // The data for our dataset
    data: {
        labels: label,
        datasets: [{
            label: 'My First dataset',
            backgroundColor: ["#3e95cd","#8e5ea2","#3cba9f"],
            borderColor: 'rgb(255, 99, 132)',
            data: numericData
        }]
    },

    // Configuration options go here
    options: {
        title:{
            display:true,
            text:"Project Status",
        }
    }
});

function decodeHTML(html) {
    var txt = document.createElement("textarea");
    txt.innerHTML = html;
    return txt.value;

}