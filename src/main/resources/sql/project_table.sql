CREATE TABLE public.project
(
    projectid bigint NOT NULL DEFAULT nextval('project_seq'::regclass),
    name character varying(100) COLLATE pg_catalog."default" NOT NULL,
    stage character varying(100) COLLATE pg_catalog."default" NOT NULL,
    description character varying(500) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT project_pkey PRIMARY KEY (projectid)
)

TABLESPACE pg_default;

ALTER TABLE public.project
    OWNER to postgres;