CREATE TABLE public.employee
(
    employeeid bigint NOT NULL DEFAULT nextval('employee_seq'::regclass),
    email character varying(100) COLLATE pg_catalog."default" NOT NULL,
    firstname character varying(100) COLLATE pg_catalog."default" NOT NULL,
    lastname character varying(100) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT employee_pkey PRIMARY KEY (employeeid)
)

TABLESPACE pg_default;

ALTER TABLE public.employee
    OWNER to postgres;