-- Table: public.project_employer

-- DROP TABLE public.project_employer;

CREATE TABLE public.project_employer
(
    project_id bigint,
    employee_id bigint,
    CONSTRAINT project_employer_employeeid_fkey FOREIGN KEY (employee_id)
        REFERENCES public.employee (employeeid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT project_employer_projectid_fkey FOREIGN KEY (project_id)
        REFERENCES public.project (projectid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

ALTER TABLE public.project_employer
    OWNER to postgres;