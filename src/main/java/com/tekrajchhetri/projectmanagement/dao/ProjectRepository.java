package com.tekrajchhetri.projectmanagement.dao;

import com.tekrajchhetri.projectmanagement.dto.ChartData;
import com.tekrajchhetri.projectmanagement.entities.Project;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface ProjectRepository extends PagingAndSortingRepository<Project, Long> {
    @Override
    public List<Project> findAll();

    @Query(nativeQuery = true , value= "SELECT COUNT(projectid) as projectCount, stage FROM project GROUP BY stage")
    public List<ChartData> projectcounts();

}
