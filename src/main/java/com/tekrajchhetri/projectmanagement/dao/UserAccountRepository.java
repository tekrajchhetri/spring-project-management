package com.tekrajchhetri.projectmanagement.dao;

import com.tekrajchhetri.projectmanagement.entities.UserAccount;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface UserAccountRepository extends PagingAndSortingRepository<UserAccount, Long> {
}
