package com.tekrajchhetri.projectmanagement.dao;

import com.tekrajchhetri.projectmanagement.dto.EmployerProject;
import com.tekrajchhetri.projectmanagement.entities.Employee;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface EmployeeRepository extends PagingAndSortingRepository<Employee, Long> {
    @Override
    public List<Employee> findAll();

    @Query(nativeQuery = true, value = "SELECT e.firstname as firstname, e.lastname as lastname, count(pe.employee_id) as projectCount from project_employer pe, employee e where e.employeeid = pe.employee_id GROUP BY e.firstname, e.lastname ORDER BY  projectCount DESC")
    public List<EmployerProject> employeeProjects();

    public Employee findByEmail(String value);
}
