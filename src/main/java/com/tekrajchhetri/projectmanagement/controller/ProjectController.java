package com.tekrajchhetri.projectmanagement.controller;

import com.tekrajchhetri.projectmanagement.dao.EmployeeRepository;
import com.tekrajchhetri.projectmanagement.dao.ProjectRepository;
import com.tekrajchhetri.projectmanagement.entities.Employee;
import com.tekrajchhetri.projectmanagement.entities.Project;
import com.tekrajchhetri.projectmanagement.services.EmployeeServices;
import com.tekrajchhetri.projectmanagement.services.ProjectServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
@RequestMapping("/project")
public class ProjectController {

    @Autowired
    ProjectServices projectServices;

    @Autowired
    EmployeeServices employeeServices;

    @GetMapping("/new")
    public String displayProjectForms(Model model){
        List<Employee> employee = employeeServices.getAll();
        model.addAttribute("allEmployee", employee);
        model.addAttribute("project", new Project());
        return "projects/new-project";
    }

    @PostMapping("/save")
    public String createProjectForms(Model model,  Project project){
        //saving data to db
        projectServices.save(project);
        //user redirected to prevent duplicate submission
        return "redirect:/project/";
    }

    @GetMapping("/")
    public String displayMainPage(Model model){

        List<Project> projectList = projectServices.getAll();
        model.addAttribute("projectList",projectList);
        return "projects/project-main";
    }
}
