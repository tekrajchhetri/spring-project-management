package com.tekrajchhetri.projectmanagement.controller;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

@Controller
public class ErrorController implements org.springframework.boot.web.servlet.error.ErrorController {

    @GetMapping("/error")
    public String handleError(HttpServletRequest request){
        Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
        if(status != null){
            Integer statuscode = Integer.valueOf(status.toString());

            if (statuscode == HttpStatus.NOT_FOUND.value()){
                return "errorpages/error-404";
            }
            else if (statuscode == HttpStatus.INTERNAL_SERVER_ERROR.value()){
                return "errorpages/error-500";
            }
            else if (statuscode == HttpStatus.FORBIDDEN.value()){
                return "errorpages/error-403";
            }
        }
        return "errorpages/error";
    }
    @Override
    public String getErrorPath() {
        return "/error";
    }


}
