package com.tekrajchhetri.projectmanagement.controller;

import com.tekrajchhetri.projectmanagement.dao.EmployeeRepository;
import com.tekrajchhetri.projectmanagement.entities.Employee;
import com.tekrajchhetri.projectmanagement.services.EmployeeServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/employee")
public class EmployeeController {
    @Autowired
    EmployeeServices employeeServices;

    @GetMapping("/new")
    public String displayForms(Model model){
        model.addAttribute("employee", new Employee());
        return "employee/new-employee";
    }

    @PostMapping("/save")
    public String SaveForm(Model model, Employee employee){
        employeeServices.save(employee);
        return "redirect:/employee/new";

    }
    @GetMapping("/")
    public String displayEmployee(Model model){
        //get data from db
        List<Employee> employeeList = employeeServices.getAll();
        //add to view for binding
        model.addAttribute("employeeList",employeeList);
        return "employee/employee-main";

    }
}
