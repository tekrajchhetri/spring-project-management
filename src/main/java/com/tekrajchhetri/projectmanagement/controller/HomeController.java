package com.tekrajchhetri.projectmanagement.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tekrajchhetri.projectmanagement.dao.EmployeeRepository;
import com.tekrajchhetri.projectmanagement.dao.ProjectRepository;
import com.tekrajchhetri.projectmanagement.dto.EmployerProject;
import com.tekrajchhetri.projectmanagement.dto.ChartData;
import com.tekrajchhetri.projectmanagement.entities.Project;
import com.tekrajchhetri.projectmanagement.services.EmployeeServices;
import com.tekrajchhetri.projectmanagement.services.ProjectServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class HomeController {

    @Value("${version}")
    private String ver;
    @Autowired
    ProjectServices projectServices;

    @Autowired
    EmployeeServices employeeServices;

    @GetMapping("/")
    public String home(Model model) throws JsonProcessingException {
        Map<String, Object> map = new HashMap<>();

        List<Project> projectList = projectServices.getAll();
        model.addAttribute("projectList",projectList);

        model.addAttribute("versionNumber",ver);

        List<ChartData> chartData = projectServices.getProjectStatus();
        //convert projectData to object json to use in javascript
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonString = objectMapper.writeValueAsString(chartData);
        model.addAttribute("jsondata", jsonString);

        List<EmployerProject> employeeprojectList = employeeServices.employerProjects();
        model.addAttribute("employeeprojectList",employeeprojectList);
        return "main/home";
    }
}
