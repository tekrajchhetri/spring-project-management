package com.tekrajchhetri.projectmanagement.dto;

public interface ChartData {
    public int getProjectCount();
    public String getStage();
}
