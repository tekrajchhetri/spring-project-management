package com.tekrajchhetri.projectmanagement.dto;

public interface EmployerProject {
    //we need to have property begin with get
    public String getFirstname();
    public String getLastname();
    public int getProjectCount();
}
