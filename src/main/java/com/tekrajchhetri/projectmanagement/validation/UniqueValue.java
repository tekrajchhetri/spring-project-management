package com.tekrajchhetri.projectmanagement.validation;
import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target(FIELD)
@Retention(RUNTIME)
@Constraint(validatedBy = UniqueValidator.class)
public @interface UniqueValue {
    String message() default "unique constraint voildated";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
