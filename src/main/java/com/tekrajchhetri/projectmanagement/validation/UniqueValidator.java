package com.tekrajchhetri.projectmanagement.validation;

import com.tekrajchhetri.projectmanagement.dao.EmployeeRepository;
import com.tekrajchhetri.projectmanagement.entities.Employee;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class UniqueValidator implements ConstraintValidator<UniqueValue, String>{

    @Autowired
    EmployeeRepository employeeRepository;


    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        Employee emp = employeeRepository.findByEmail(s);
        if(emp!=null) return false;
        else return true;
    }
}
