package com.tekrajchhetri.projectmanagement.api.controllers;

import com.tekrajchhetri.projectmanagement.dao.ProjectRepository;
import com.tekrajchhetri.projectmanagement.entities.Employee;
import com.tekrajchhetri.projectmanagement.entities.Project;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/projects")
public class ProjectAPIController {
    @Autowired
    ProjectRepository projectRepository;

    @GetMapping
    public List<Project> getProject(){
        return projectRepository.findAll();
    }

    @GetMapping("/{id}")
    public Project getProjectById(@PathVariable("id") Long id){
        return projectRepository.findById(id).get();
    }

    @PostMapping(consumes = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public Project create(@RequestBody @Valid Project project){
        return projectRepository.save(project);
    }

    @PutMapping(consumes = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public Project update(@RequestBody @Valid Project project){
        return projectRepository.save(project);
    }

    @PatchMapping(path = "/{id}", consumes =  "application/json")
    @ResponseStatus(HttpStatus.OK)
    public Project partialUpdate(@PathVariable("id") Long id, @RequestBody @Valid Project patchProject){
        Project proj = projectRepository.findById(id).get();
        if(patchProject.getName() != null){
            proj.setName(patchProject.getName());
        }
        if(patchProject.getDescription() != null){
            proj.setDescription(patchProject.getDescription());
        }
        if(patchProject.getStage() != null){
            proj.setStage(patchProject.getStage());
        }
        return projectRepository.save(proj);
    }

    @DeleteMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable("id") Long id){
        try{
            projectRepository.deleteById(id);
        }catch (Exception ex){}
    }

    @GetMapping(params = {"page", "size"})
    @ResponseStatus(HttpStatus.OK)
    public Iterable<Project> paginatedProject(@RequestParam("page") int page, @RequestParam("size") int size){
        Pageable pageable = PageRequest.of(page,size);
        return projectRepository.findAll(pageable);
    }
}
