package com.tekrajchhetri.projectmanagement.api.controllers;

import com.tekrajchhetri.projectmanagement.dao.EmployeeRepository;
import com.tekrajchhetri.projectmanagement.entities.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/employees")
public class EmployeeAPIController {
    @Autowired
    EmployeeRepository employeeRepository;

    @GetMapping
    public List<Employee> getEmployee(){
        return employeeRepository.findAll();
    }

    @GetMapping("/{id}")
    public Employee getEmployeeById(@PathVariable("id") Long id){
        return employeeRepository.findById(id).get();
    }

    @PostMapping(consumes = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public Employee create(@RequestBody @Valid Employee employee){
        return employeeRepository.save(employee);
    }

    @PutMapping(consumes = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public Employee update(@RequestBody @Valid Employee employee){
        return employeeRepository.save(employee);
    }

    @PatchMapping(path = "/{id}", consumes =  "application/json")
    @ResponseStatus(HttpStatus.OK)
    public Employee partialUpdate(@PathVariable("id") Long id, @RequestBody @Valid Employee patchEmployee){
        Employee emp = employeeRepository.findById(id).get();
        if(patchEmployee.getEmail() != null){
            emp.setEmail(patchEmployee.getEmail());
        }
        if(patchEmployee.getFirstname() != null){
            emp.setFirstname(patchEmployee.getFirstname());
        }
        if(patchEmployee.getLastname() != null){
            emp.setLastname(patchEmployee.getLastname());
        }
        return employeeRepository.save(emp);
    }

    @DeleteMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable("id") Long id){
        try{
            employeeRepository.deleteById(id);
        }catch (Exception ex){}
    }

    @GetMapping(params = {"page", "size"})
    @ResponseStatus(HttpStatus.OK)
    public Iterable<Employee> PaginatedEmployee(@RequestParam("page") int page, @RequestParam("size") int size){
        Pageable pageable = PageRequest.of(page,size);
        return employeeRepository.findAll(pageable);
    }
}
