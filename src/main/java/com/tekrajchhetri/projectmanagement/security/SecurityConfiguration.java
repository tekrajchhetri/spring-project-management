package com.tekrajchhetri.projectmanagement.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    DataSource dataSource;

    @Autowired
    WebConfig bcryptEncoder;

    @Override
    protected void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        authenticationManagerBuilder.jdbcAuthentication()
        .usersByUsernameQuery("SELECT username, password, enabled FROM user_accounts where username = ?")
        .authoritiesByUsernameQuery("SELECT username, role FROM user_accounts where username = ?")
        .dataSource(dataSource)
        .passwordEncoder(bcryptEncoder.passwordEncoder());
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception{
        httpSecurity.authorizeRequests()
                .antMatchers("/project/new").hasRole("ADMIN")
                .antMatchers("/project/save").hasRole("ADMIN")
                .antMatchers("/employee/new").hasRole("ADMIN")
                .antMatchers("/employee/save").hasRole("ADMIN")
                .antMatchers("/","/**").permitAll()
                .and()
                .formLogin();
//        httpSecurity.csrf().disable();

    }
}
