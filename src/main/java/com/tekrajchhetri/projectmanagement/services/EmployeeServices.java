package com.tekrajchhetri.projectmanagement.services;

import com.tekrajchhetri.projectmanagement.dao.EmployeeRepository;
import com.tekrajchhetri.projectmanagement.dto.EmployerProject;
import com.tekrajchhetri.projectmanagement.entities.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class EmployeeServices {

    @Autowired
    EmployeeRepository employeeRepository;

    public Employee save(Employee employee){
        return employeeRepository.save(employee);
    }

    public List<Employee> getAll(){
        return employeeRepository.findAll();
    }

    public List<EmployerProject> employerProjects(){
        return employeeRepository.employeeProjects();
    }
}
