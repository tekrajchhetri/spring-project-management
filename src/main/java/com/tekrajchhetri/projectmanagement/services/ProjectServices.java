package com.tekrajchhetri.projectmanagement.services;

import com.tekrajchhetri.projectmanagement.dao.ProjectRepository;
import com.tekrajchhetri.projectmanagement.dto.ChartData;
import com.tekrajchhetri.projectmanagement.entities.Project;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProjectServices {

    @Autowired
    ProjectRepository projectRepository;

    public Project save(Project project){
        return projectRepository.save(project);
    }

    public List<Project> getAll(){
        return projectRepository.findAll();
    }

    public  List<ChartData> getProjectStatus(){
        return projectRepository.projectcounts();
    }
}
