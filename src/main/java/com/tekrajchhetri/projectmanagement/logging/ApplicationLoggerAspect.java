package com.tekrajchhetri.projectmanagement.logging;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Aspect
@Component
public class ApplicationLoggerAspect {
    private final Logger logger =  LoggerFactory.getLogger(this.getClass());

    @Pointcut("within(com.tekrajchhetri.projectmanagement.controller..*) || within(com.tekrajchhetri.projectmanagement.dao..*)")
    public void definePackagePointcut(){
        //empty method to define pointcut location

    }
    @Around("definePackagePointcut()")
    public Object logAround(ProceedingJoinPoint jointPoint){
        logger.info("\n\n\n");
        logger.info("********************************************** Before METHOD EXECUTION ***************************************************** \n");
        logger.info(jointPoint.getSignature().getDeclaringType()+":"+jointPoint.getSignature().getName()+":"+Arrays.toString(jointPoint.getArgs()));
        logger.info("\n\n___________________________________________________________________________________________________________\n\n");

        Object o = null;
        try {
            o = jointPoint.proceed();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        logger.info("\n\n\n");
        logger.info("********************************************** AFTER METHOD EXECUTION ***************************************************** \n");
        logger.info(jointPoint.getSignature().getDeclaringType()+":"+jointPoint.getSignature().getName()+":"+Arrays.toString(jointPoint.getArgs()));
        logger.info("\n\n___________________________________________________________________________________________________________\n\n");

        return o;
    }
}
